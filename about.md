---
layout: default
---

## About

This is the personal blog of Colin Bradley, freelance technologist and small businessman ("small business" man rather than a diminutive man of business).

### Stuff and interests

  * Partner in the delightful wedding invitation business [The Vintage Invite Company](http://thevintageinvite.com)
  * .NET developer in the finance business
  * Ruby on Rails hacker for some private (for now) side projects

### Acknowledgements

  * *Artwork (logo)*: [Dan Morley](http://www.danmorley.com/)
  * *CSS*: [Zurb Foundation](http://foundation.zurb.com/)
  * *Site Generation*: [Jekyll](http://jekyllrb.com/)
  * *Hosting*: [Github Pages](http://pages.github.com/)
  * *Comments*: [Disqus](http://disqus.com/)
